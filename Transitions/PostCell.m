//
//  PostCell.m
//  Transitions
//
//  Created by Daniel.Burke on 3/11/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "PostCell.h"

@implementation PostCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _postImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _postImageView.contentMode = UIViewContentModeScaleAspectFill;
        _postImageView.clipsToBounds = YES;
        [self addSubview:_postImageView];
        
        _postTitleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 59, self.frame.size.width, 44)];
        _postTitleTextView.textColor = [UIColor whiteColor];
        _postTitleTextView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        _postTitleTextView.text = @"This is a moderately long post title";
        _postTitleTextView.editable = NO;
        [self addSubview:_postTitleTextView];
        
        _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
        [self addGestureRecognizer:_longPressRecognizer];
    }
    return self;
}

-(void)handleLongPressGesture:(UILongPressGestureRecognizer*)recognizer{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"Display Shade, Add Pan Gesture (which will handle transition");
    }
}

@end
