//
//  PostCell.h
//  Transitions
//
//  Created by Daniel.Burke on 3/11/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCell : UICollectionViewCell

@property (copy, nonatomic) UILongPressGestureRecognizer *longPressRecognizer;
@property (copy, nonatomic) UIImageView *postImageView;
@property (copy, nonatomic) UITextView *postTitleTextView;

@end
