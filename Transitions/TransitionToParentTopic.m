//
//  TransitionToParentTopic.m
//  Transitions
//
//  Created by Daniel.Burke on 3/11/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "TransitionToParentTopic.h"
#import "ParentViewController.h"
#import "ChildViewController.h"
#import "PostCell.h"

@implementation TransitionToParentTopic

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    ChildViewController *fromViewController = (ChildViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    ParentViewController *toViewController = (ParentViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    NSTimeInterval duration = [self transitionDuration:transitionContext];

    // Get a snapshot of the image view
    UIView *imageSnapshot = [fromViewController.postImageView snapshotViewAfterScreenUpdates:NO];
    imageSnapshot.frame = [containerView convertRect:fromViewController.postImageView.frame fromView:fromViewController.postImageView.superview];
    fromViewController.postImageView.hidden = YES;
    
    // Get a snapshot of the thing cell we're transitioning from
    PostCell *cell = (PostCell*)[toViewController.postsCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:fromViewController.postIndex inSection:0]];
    cell.postImageView.hidden = YES;
    
    // Setup the initial view states
    toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
    [containerView insertSubview:toViewController.view belowSubview:fromViewController.view];
    [containerView addSubview:imageSnapshot];
    
    fromViewController.view.alpha = 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        
        // Move the image view
        imageSnapshot.frame = [containerView convertRect:cell.postImageView.frame fromView:cell.postImageView.superview];
        
        
    } completion:^(BOOL finished) {
        // Clean up
        fromViewController.postImageView.hidden = NO;
        cell.postImageView.hidden = NO;
        [imageSnapshot removeFromSuperview];
        
        // Declare that we've finished
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

@end
