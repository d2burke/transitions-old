//
//  TransitionToChildPost.h
//  Transitions
//
//  Created by Daniel.Burke on 3/11/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitionToChildPost : NSObject<UIViewControllerAnimatedTransitioning>

@end
