//
//  TransitionToChildPost.m
//  Transitions
//
//  Created by Daniel.Burke on 3/11/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "TransitionToChildPost.h"
#import "ParentViewController.h"
#import "ChildViewController.h"
#import "PostCell.h"

@implementation TransitionToChildPost

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    ParentViewController *fromViewController = (ParentViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    ChildViewController *toViewController = (ChildViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    // Get a snapshot of the thing cell we're transitioning from
    PostCell *cell = (PostCell*)[fromViewController.postsCollectionView cellForItemAtIndexPath:[[fromViewController.postsCollectionView indexPathsForSelectedItems] firstObject]];
    UIView *cellImageSnapshot = [cell.postImageView snapshotViewAfterScreenUpdates:NO];
    cellImageSnapshot.frame = [containerView convertRect:cell.postImageView.frame fromView:cell.postImageView.superview];
    cell.postImageView.hidden = YES;
    
    // Setup the initial view states
    toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
    toViewController.view.alpha = 0;
    toViewController.postImageView.hidden = YES;
    
    [containerView addSubview:toViewController.view];
    [containerView addSubview:cellImageSnapshot];
    
    [UIView animateWithDuration:duration animations:^{
        // later, animate postTitleTextView to fit the new view size
        
        // Fade in the second view controller's view
        toViewController.view.alpha = 1.0;
        
        // Move the cell snapshot so it's over the second view controller's image view
        CGRect frame = [containerView convertRect:toViewController.postImageView.frame fromView:toViewController.contentScrollView];
        cellImageSnapshot.frame = frame;
    } completion:^(BOOL finished) {
        // Clean up
        toViewController.postImageView.hidden = NO;
        cell.postImageView.hidden = NO;
        [cellImageSnapshot removeFromSuperview];
        
        // Declare that we've finished
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

@end
