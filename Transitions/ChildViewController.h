//
//  ChildViewController.h
//  Transitions
//
//  Created by Daniel.Burke on 3/10/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildViewController : UIViewController
<
    UINavigationControllerDelegate,
    UIScrollViewDelegate
>

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactivePopTransition;

//Data
@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) CGFloat viewHeight;
@property (nonatomic) int postIndex;

//UI Elements
@property (strong, nonatomic) UIScrollView *contentScrollView;
@property (strong, nonatomic) UIImageView *postImageView;
@property (strong, nonatomic) UIImage *postImage;
@property (strong, nonatomic) UITextView *postTitleTextView;

@end
