//
//  ChildViewController.m
//  Transitions
//
//  Created by Daniel.Burke on 3/10/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "ChildViewController.h"
#import "ParentViewController.h"
#import "TransitionToParentTopic.h"

@interface ChildViewController ()

@end

@implementation ChildViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

-(void)viewDidAppear:(BOOL)animated{
    self.navigationController.delegate = self;
    _postImageView.image = _postImage;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
	[self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
	self.navigationController.navigationBar.shadowImage = [UIImage new];
	self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    // Stop being the navigation controller's delegate
    if (self.navigationController.delegate == self) {
        self.navigationController.delegate = nil;
    }
}

#pragma mark - UI
-(void)initUI{
    _viewWidth = self.view.frame.size.width;
    _viewHeight = self.view.frame.size.height;
    
    _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    _contentScrollView.alwaysBounceVertical = YES;
    _contentScrollView.delegate = self;
    [self.view addSubview:_contentScrollView];
    
    _postImageView = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, _viewWidth - 2, 199)];
    _postImageView.backgroundColor = [UIColor blueColor];
    _postImageView.contentMode = UIViewContentModeScaleAspectFill;
    _postImageView.clipsToBounds = YES;
    _postImageView.userInteractionEnabled = YES;
    [_contentScrollView addSubview:_postImageView];
    
    _postTitleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 201, _viewWidth, 40)];
    _postTitleTextView.textColor = [UIColor blackColor];
    _postTitleTextView.backgroundColor = [UIColor lightGrayColor];
    _postTitleTextView.text = @"Jet turned off course, Malaysian air force says";
    [_contentScrollView addSubview:_postTitleTextView];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat y = -1 * scrollView.contentOffset.y;
    NSLog(@"Released: %f", y);
    if(y > 150){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UINavigationControllerDelegate methods

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    // Check if we're transitioning from this view controller to a DSLFirstViewController
    if (fromVC == self && [toVC isKindOfClass:[ParentViewController class]]) {
        return [[TransitionToParentTopic alloc] init];
    }
    else {
        return nil;
    }
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                         interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    // Check if this is for our custom transition
    if ([animationController isKindOfClass:[TransitionToParentTopic class]]) {
        return self.interactivePopTransition;
    }
    else {
        return nil;
    }
}


#pragma mark UIGestureRecognizer handlers

-(void)handlePanRecognizer:(UIGestureRecognizer*)recognizer{
    CGFloat progress = [recognizer locationInView:self.view].y / (self.view.bounds.size.height * 1.0);
//    NSLog(@"Offset: %f", progress);
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Create a interactive transition and pop the view controller
        self.interactivePopTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Update the interactive transition's progress
        [self.interactivePopTransition updateInteractiveTransition:progress];
        
        if (progress > 0.5) {
            [self.interactivePopTransition finishInteractiveTransition];
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        if (progress > 0.5) {
            [self.interactivePopTransition finishInteractiveTransition];
        }
        else {
            [self.interactivePopTransition cancelInteractiveTransition];
        }
        
        self.interactivePopTransition = nil;
    }
}

- (void)handlePopRecognizer:(UIScreenEdgePanGestureRecognizer*)recognizer {
    CGFloat progress = [recognizer translationInView:self.view].x / (self.view.bounds.size.width * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
    
    
    NSLog(@"Progress: %f", progress);
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Create a interactive transition and pop the view controller
        self.interactivePopTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Update the interactive transition's progress
        [self.interactivePopTransition updateInteractiveTransition:progress];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        if (progress > 0.5) {
            [self.interactivePopTransition finishInteractiveTransition];
        }
        else {
            [self.interactivePopTransition cancelInteractiveTransition];
        }
        
        self.interactivePopTransition = nil;
    }
    
}

@end
