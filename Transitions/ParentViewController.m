//
//  ParentViewController.m
//  Transitions
//
//  Created by Daniel.Burke on 3/10/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import "ParentViewController.h"
#import "ChildViewController.h"
#import "TransitionToChildPost.h"
#import "PostCell.h"

@interface ParentViewController ()

@end

@implementation ParentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _postImages = [@[@"crimea",@"fallon",@"flight",@"healthcaregov",@"mvp",@"pistorius",@"snowden"] mutableCopy];
    [self initUI];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewDidAppear:(BOOL)animated{
    self.navigationController.delegate = self;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
	[self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
	self.navigationController.navigationBar.shadowImage = [UIImage new];
	self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    // Stop being the navigation controller's delegate
    if (self.navigationController.delegate == self) {
        self.navigationController.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Transition Methods

#pragma mark UINavigationControllerDelegate methods

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    // Check if we're transitioning from this view controller to a DSLSecondViewController
    if (fromVC == self && [toVC isKindOfClass:[ChildViewController class]]) {
        return [[TransitionToChildPost alloc] init];
    }
    else {
        return nil;
    }
}

#pragma mark - Init UI
-(void)initUI{
    _viewWidth = self.view.frame.size.width;
    _viewHeight = self.view.frame.size.height;
    
    _topicImageView = [[UIImageView alloc] initWithFrame:CGRectMake(1, 0, _viewWidth - 2, 200)];
    _topicImageView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:_topicImageView];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 1, 1, 1);
    CGRect collectionFrame = self.view.frame;
    collectionFrame.origin.y = 201;
    collectionFrame.size.height = self.view.frame.size.height - 201;
    _postsCollectionView = [[UICollectionView alloc] initWithFrame:collectionFrame collectionViewLayout:layout];
    _postsCollectionView.delegate = self;
    _postsCollectionView.dataSource = self;
    
    [_postsCollectionView registerClass:[PostCell class] forCellWithReuseIdentifier:@"Post"];
    _postsCollectionView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_postsCollectionView];
}

#pragma mark UICollectionViewDelegate Methods
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ChildViewController *childVC = [[ChildViewController alloc] init];
    childVC.view.backgroundColor = [UIColor whiteColor];
    childVC.title = @"Child View";
    NSString *imageName = [NSString stringWithFormat:@"%@.jpg", [_postImages objectAtIndex:indexPath.row]];
    childVC.postImage = [UIImage imageNamed:imageName];
    childVC.postIndex = indexPath.row;
    [self.navigationController pushViewController:childVC animated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PostCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"Post" forIndexPath:indexPath];
    cell.postImageView.backgroundColor = [UIColor blueColor];
    NSString *imageName = [NSString stringWithFormat:@"%@.jpg", [_postImages objectAtIndex:indexPath.row]];
    cell.postImageView.image = [UIImage imageNamed:imageName];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(158, 99);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,1,1,1);  // top, left, bottom, right
}

@end
