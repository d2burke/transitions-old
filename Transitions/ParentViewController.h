//
//  ParentViewController.h
//  Transitions
//
//  Created by Daniel.Burke on 3/10/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentViewController : UIViewController

<
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout,
    UIScrollViewDelegate,
    UINavigationControllerDelegate,
    UIGestureRecognizerDelegate
>

//Data
@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) CGFloat viewHeight;

//UI
@property (strong, nonatomic) UIImageView *topicImageView;
@property (strong, nonatomic) UICollectionView *postsCollectionView;



//Test Elements
@property (strong, nonatomic) NSMutableArray *postImages;

@end
