# iOS Custom ViewController Transitions #

This is a basic example of UIViewController Animated Transitioning.  The app displays a collection of cells which, when tapped, animate up to a single view controller.